import { parse } from "@unified-latex/unified-latex-util-parse";

import type { Arg, Content, TypeRoot } from "./LexerTypes";

/**
 * Extracts the title from the LaTeX file.
 *
 * @param root The root of the abstract syntax tree.
 * @returns The title.
 */
const getTitle = (root: TypeRoot): string => {
  const content = root.content
    /*
    Filter out the abstract syntax tree to find
      the nodes that are the title element that 
      has args that are not undefined.
  */
    .filter((x: Content) => {
      return (
        typeof x.content === "string" &&
        x.content?.includes("title") &&
        x.args != undefined
      );
    })
    /*
      Map over the filtered array to get the 
        content from the args.
    */
    .map((x: Content) =>
      /*
        Map over each content to filter content
          type that are only strings. And then
          map over the contents and converts it
          to a string if it is not already.
          Then join the array of strings together.
      */
      x.args?.map((y: Arg) =>
        y.content
          .filter((z) => z.type == "string")
          .map((z) => `${z.content}`)
          .join(" ")
      )
    )
    /*
      This may return an array of arrays of strings
        so we flat the array to get an array of strings.
    */
    .flat()
    /*
      Filter out the empty strings from the array.
    */
    .filter((x) => x?.trim() != "");

  /* 
    If the array is empty return an empty string
      else return the first element of the array.
  */
  return content?.[0] ?? "";
};

/**
 * Extracts the author from the LaTeX file.
 *
 * @param root The root of the abstract syntax tree.
 * @returns The author.
 */
const getAuthor = (root: TypeRoot): string => {
  let content =
    root.content
      /*
        Filter out the abstract syntax tree to find
          the nodes that are the author element.
      */
      .filter(
        (x: Content) =>
          typeof x.content === "string" && x.content?.includes("author")
      )
      /*
        Map over the filtered array to get the 
          content from the args.
      */
      .map((x) =>
        /*
          Map over each content element to filter content
            type that are only strings or macros
            if it is a string it will return the content as a string
            else it will return a backslash which im using as a delimiter.
            Then join the array of strings together.
        */
        x.args?.map((y) =>
          y.content
            .filter((z) => z.type == "string" || z.type == "macro")
            .map((z) => {
              if (z.type == "string") {
                return `${z.content}`;
              }
            })
            .join(" ")
        )
      )
      /*
        This may return an array of arrays of strings
          so we flat the array to get an array of strings.
      */
      .flat()
      /*
        Filter out the empty strings from the array.
      */
      .filter((x) => x != undefined && x.trim() != "")
      ?.at(0)
      /*
      split the string by the backslash delimiter
      */
      ?.split("\\")
      .at(0)
      ?.trim() ?? "";
  /*
    Replace the commas with extra spaces and the "and"'s
      with a semicolon to make the string easier to parse.
  */
  content = content.replace(" , ", "; ");
  content = content.replace(" and ", "; ");

  return content;
};

/**
 * Extracts the abstract from the LaTeX file.
 *
 * @param root The root of the abstract syntax tree.
 * @returns The abstract.
 */
export const getAbstract = (root: TypeRoot): string => {
  const content = root.content
    /*
    Filter out the abstract syntax tree to find
      the nodes that are of type environment 
      with a document env.
  */
    .filter((x) => {
      const isEnv = x.type == "environment";
      const isDoc = x.env == "document";
      return isEnv && isDoc;
    })
    /*
      Map over the filtered array to get the contents.
    */
    .map((x) => x.content)
    /*
      This may return an array of arrays of contents
        so we flat the array to get an array of contents.
    */
    .flat()
    /*
      Filter out the contents that are of type environment
        and has a env of either abstract or sciabstract.

        */

    .filter((x: Content) => {
      const isEnv = x.type == "environment";
      const isAbs = x.env == "abstract";
      const isSciAbs = x.env == "sciabstract";

      /* v8 ignore next */
      return isEnv && (isAbs || isSciAbs);
    }) as Content[];

  /*
    If the array is empty return an empty string
      else return the first element of the array.
  */
  if (content.length != 0) {
    /*
      Skipping the v8 code voerage as because it wats to to test if the 
        content is undefined which it will never be, as we already checked
        that the content is not empty.
    */
    /* v8 ignore next */
    const contentToMap1 = content.at(0)?.content as Content[] | undefined;

    if (contentToMap1 != undefined && contentToMap1.length > 0) {
      /*
        Map over the content to filter content type
          if it is a string it will return the content as a string
          else if it is a macro it will return a backslash 
            which im using as a delimiter.
          otherwise it will return an empty string.
      */

      let ContentValArr: string[] = [
        ...contentToMap1
          .map((z: Content) => {
            if (z.type == "string") {
              return `${z.content}`;
            } else return "";
          })
          /*
          Filter out the empty strings from the array.
        */
          .filter((x) => x.trim() != "")
          /*
          Join the array of strings together.
          then split the string by the backslash delimiter
        */
          .join(" ")
          .split("\\"),
      ];
      /*
        if the string is undefined return an empty string
      */
      /*
        Skipping the v8 code see test 10 for more information.~
      */
      /* v8 ignore next */
      if (ContentValArr?.at(0) == undefined) return "";

      let ContentVal: string = `${ContentValArr.at(0)}`;
      /*
        Removes extra spaces from around many characters.
      */
      ContentVal = ContentVal.split(" ")
        .filter((x) => x.trim() != "")
        .join(" ");
      ContentVal = ContentVal.trim();
      ContentVal = ContentVal.replaceAll(" .", ".");
      ContentVal = ContentVal.replaceAll(" ' ", "'");
      ContentVal = ContentVal.replaceAll(" ,", ",");
      ContentVal = ContentVal.replaceAll("( ", "(");
      ContentVal = ContentVal.replaceAll(" )", ")");
      return ContentVal;
    }
  }

  return "";
};

/**
 * Extracts the title, authors, and abstract from a LaTeX file.
 *
 * @param fileData The LaTeX file dat`a.
 * @returns The title, authors, and abstract.
 */
export const ExtractUploadInfo = (fileData: string) => {
  const lexedOut = parse(fileData) as TypeRoot;

  return {
    title: getTitle(lexedOut),
    authors: getAuthor(lexedOut),
    abstract: getAbstract(lexedOut),
  };
};
