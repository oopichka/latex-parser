export interface TypeRoot {
  type: string;
  content: Content[];
  position: Position;
}

export interface Content {
  type: string;
  content?: any;
  sameline?: boolean;
  leadingWhitespace?: boolean;
  position: Position;
  suffixParbreak?: boolean;
  _renderInfo?: RenderInfo;
  args?: Arg[];
  env?: string;
}

export interface Position {
  start: Start;
  end: End;
}

export interface Start {
  offset: number;
  line: number;
  column: number;
}

export interface End {
  offset: number;
  line: number;
  column: number;
}

export interface RenderInfo {
  breakAround?: boolean;
  namedArguments?: string | undefined[];
  inParMode?: boolean;
  breakAfter?: boolean;
  pgfkeysArgs?: boolean;
}

export interface Arg {
  type: string;
  content: Content[];
  openMark: string;
  closeMark: string;
}
