# Latex Parser

To install dependencies:

```bash
bun install
```

To run:

```bash
bun run
```

To test:

```bash
bun test
```
