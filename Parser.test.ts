import { parse } from "@unified-latex/unified-latex-util-parse";
import { expect, test } from "vitest";
import type { TypeRoot } from "./LexerTypes";
import { ExtractUploadInfo, getAbstract } from "./Parser";

test('ParseTex: "Normal" LaTeX Document', () => {
  // Arrange
  const LaTex = `
    \\documentclass[letterpaper,twocolumn,twoside]{article}

    \\title{This is a test LaTex document}

    \\author{Test Author 1, Test Author 2 and Test Author 3}

    \\begin{document}


    \\begin{abstract}
        
        This is a test abstract.

        % \\textbf{Key Words}: stock price prediction; LSTM; Indian Stock Market; Hybrid mutual fund; investment portfolio management.

    \\end{abstract}
    \\end{document}
    ` as const;

  // Act
  const ParsedTex = ExtractUploadInfo(LaTex);

  // Assert
  expect(ParsedTex.title).toBe("This is a test LaTex document");
  expect(ParsedTex.authors).toBe("Test Author 1; Test Author 2; Test Author 3");
  expect(ParsedTex.abstract).toBe("This is a test abstract.");
});

test("ParseTex: No title, authors, or abstract feilds", () => {
  // Arrange
  const LaTex = `
    \\documentclass[letterpaper,twocolumn,twoside]{article}

    \\begin{document}

    \\end{document}
    ` as const;

  // Act
  const ParsedTex = ExtractUploadInfo(LaTex);

  // Assert
  expect(ParsedTex.title).toBe("");
  expect(ParsedTex.authors).toBe("");
  expect(ParsedTex.abstract).toBe("");
});

test("ParseTex: Empty Document", () => {
  // Arrange
  const LaTex = `` as const;

  // Act
  const ParsedTex = ExtractUploadInfo(LaTex);

  // Assert
  expect(ParsedTex.title).toBe("");
  expect(ParsedTex.authors).toBe("");
  expect(ParsedTex.abstract).toBe("");
});

test("ParseTex: Two Title Sections", () => {
  // Arrange
  const LaTex = `
    \\documentclass[letterpaper,twocolumn,twoside]{article}

    \\title{Test Title 1}
    \\title{Test Title 2}

    \\author{Test Author 1, Test Author 2 and Test Author 3}

    \\begin{document}


    \\begin{abstract}
        
        This is a test abstract.

    \\end{abstract}
    \\end{document}
    ` as const;

  // Act
  const ParsedTex = ExtractUploadInfo(LaTex);

  // Assert
  expect(ParsedTex.title).toBe("Test Title 1");
  expect(ParsedTex.authors).toBe("Test Author 1; Test Author 2; Test Author 3");
  expect(ParsedTex.abstract).toBe("This is a test abstract.");
});

test("ParseTex: Two Author Sections", () => {
  // Arrange
  const LaTex = `
      \\documentclass[letterpaper,twocolumn,twoside]{article}
  
      \\title{Test Title 1}
  
      \\author{Test Author 1, Test Author 2 and Test Author 3}
      \\author{Test Author 4, Test Author 5 and Test Author 6}
  
      \\begin{document}
  
  
      \\begin{abstract}
          
          This is a test abstract.
    
      \\end{abstract}
      \\end{document}
      ` as const;

  // Act
  const ParsedTex = ExtractUploadInfo(LaTex);

  // Assert
  expect(ParsedTex.title).toBe("Test Title 1");
  expect(ParsedTex.authors).toBe("Test Author 1; Test Author 2; Test Author 3");
  expect(ParsedTex.abstract).toBe("This is a test abstract.");
});

test("ParseTex: Two Abstracts Sections", () => {
  // Arrange
  const LaTex = `
      \\documentclass[letterpaper,twocolumn,twoside]{article}
  
      \\title{Test Title 1}
  
      \\author{Test Author 1, Test Author 2 and Test Author 3}
      \\author{Test Author 4, Test Author 5 and Test Author 6}
  
      \\begin{document}
  
  
      \\begin{abstract}
          
        This is test abstract 1.
  
      \\end{abstract}
      \\begin{abstract}
          
      This is test abstract 2.

      \\end{abstract}
      \\end{document}
      ` as const;

  // Act
  const ParsedTex = ExtractUploadInfo(LaTex);

  // Assert
  expect(ParsedTex.title).toBe("Test Title 1");
  expect(ParsedTex.authors).toBe("Test Author 1; Test Author 2; Test Author 3");
  expect(ParsedTex.abstract).toBe("This is test abstract 1.");
});

test("ParseTex: EmptyFile", () => {
  // Arrange
  const LaTex = `` as const;

  // Act
  const ParsedTex = ExtractUploadInfo(LaTex);

  // Assert
  expect(ParsedTex.title).toBe("");
  expect(ParsedTex.authors).toBe("");
  expect(ParsedTex.abstract).toBe("");
});

test("ParseTex: LatexFile that does not define the document macros", () => {
  // Arrange
  const LaTex = `

      \\title{Test Title 1}
  
      \\author{Test Author 1, Test Author 2 and Test Author 3}
  
      \\begin{abstract}
          
        This is test abstract 1.
  
      \\end{abstract}
  
      ` as const;

  // Act
  const ParsedTex = ExtractUploadInfo(LaTex);

  // Assert
  expect(ParsedTex.title).toBe("Test Title 1");
  expect(ParsedTex.authors).toBe("Test Author 1; Test Author 2; Test Author 3");
  // As the the abastract is normally defined within the document macro,
  //  the abstract should be empty as it cannot find the document macro.
  expect(ParsedTex.abstract).toBe("");
});

test("ParseTex: LatexFile with an empty string", () => {
  // Arrange
  const parser_ast: TypeRoot = {
    type: "root",
    content: [
      {
        type: "environment",
        env: "document",
        content: [
          {
            type: "environment",
            env: "abstract",
            content: [
              {
                type: "string",
                content: "",
                position: {
                  start: {
                    offset: 293,
                    line: 14,
                    column: 9,
                  },
                  end: {
                    offset: 297,
                    line: 14,
                    column: 13,
                  },
                },
              },
            ],
          },
        ],
        position: {
          start: {
            offset: 228,
            line: 9,
            column: 7,
          },
          end: {
            offset: 449,
            line: 22,
            column: 21,
          },
        },
      },
    ],
    position: {
      start: {
        offset: 0,
        line: 1,
        column: 1,
      },
      end: {
        offset: 456,
        line: 23,
        column: 7,
      },
    },
  } as const;

  // Act
  const ParsedAbsract = getAbstract(parser_ast);

  // Assert
  expect(expect(ParsedAbsract).toBe(""));
});

test(`ParseTex: with a string in the abstract set as undefined
      (This case should never occur, because if there was not 
        string between the abstract macros "\begin{abstract}\end{abstract}", 
        the string be set as an empty string, never and undefined. if the 
        abstract is not even defined in the Latex file, we can see as seen 
        in test case 2 it would be an empty string. If someone is intercepting 
        the Abstract Syntax Tree and setting the string to be undefined, we 
        expected to return a string with the work "undefined")`, () => {
  // Arrange
  const parser_ast: TypeRoot = {
    type: "root",
    content: [
      {
        type: "environment",
        env: "document",
        content: [
          {
            type: "environment",
            env: "abstract",
            content: [
              {
                type: "string",
                content: undefined,
                position: {
                  start: {
                    offset: 293,
                    line: 14,
                    column: 9,
                  },
                  end: {
                    offset: 297,
                    line: 14,
                    column: 13,
                  },
                },
              },
            ],
          },
        ],
        position: {
          start: {
            offset: 228,
            line: 9,
            column: 7,
          },
          end: {
            offset: 449,
            line: 22,
            column: 21,
          },
        },
      },
    ],
    position: {
      start: {
        offset: 0,
        line: 1,
        column: 1,
      },
      end: {
        offset: 456,
        line: 23,
        column: 7,
      },
    },
  } as const;

  // Act
  const ParsedAbsract = getAbstract(parser_ast);

  // Assert
  expect(expect(ParsedAbsract).toBe("undefined"));
});
